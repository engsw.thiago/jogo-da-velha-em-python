import time
import random
import os
from re import T
from colorama import Fore, Back, Style

#   Declarando variáveis  | por questoes de organização

playAgain = True    #Jogar Novamente
jogadas = 0         #Quantidade de Jogadas
quemJoga = 1        #1- vez da CPU , 2- minha vez
maxJogadas = 9      #Quantidade máxima de jogadas
winner = False       #Se ganhou ou perdeu
y = 0               #uma var usada apenas para imprimir a tela e Y num gráfico (x,y)
x = 0               #uma var usada apenas para imprimir a tela e X num gráfico (x,y)

#   Matriz do jogo
velha = [
    [' ',' ',' '],
    [' ',' ',' '],
    [' ',' ',' ']
]



#   Criação da tela
def tela():
    global velha
    global jogadas
    global y

    # IMPRIMINDO O QUADRO DO JOGO
    os.system('cls')
    print(Fore.MAGENTA)
    print('  ===============')
    print(Fore.YELLOW + '   JOGO DA VELHA' + Fore.RESET + Fore.MAGENTA)
    print('  ===============')
    print(Fore.RESET)

    print(Style.BRIGHT + Fore.RED + '     0   1   2' + Fore.RESET + Style.RESET_ALL)

    print(Style.BRIGHT + Fore.YELLOW + '0:   '+ Fore.RESET + Style.RESET_ALL + velha[0][0] + ' | ' + velha[0][1] + ' | ' + velha[0][2])  
    print('    ------------')
  
    print(Style.BRIGHT + Fore.YELLOW + '1:   '+ Fore.RESET + Style.RESET_ALL + velha[1][0] + ' | ' + velha[1][1] + ' | ' + velha[1][2])  
    print('    ------------')

    print(Style.BRIGHT + Fore.YELLOW + '2:   '+ Fore.RESET + Style.RESET_ALL + velha[2][0] + ' | ' + velha[2][1] + ' | ' + velha[2][2])  
    print('    ')

    print('Jogadas: ' + Style.BRIGHT + Fore.GREEN + str(jogadas) + Fore.RESET + Style.RESET_ALL)

    if quemJoga == 2:
        print('Vez do jogador:' + Style.BRIGHT + Fore.GREEN + 'HUMANO' + Fore.RESET + Style.RESET_ALL)
    else:
        print('Vez do jogador:' + Style.BRIGHT + Fore.CYAN + 'CPU' + Fore.RESET + Style.RESET_ALL)
        time.sleep(0.5)
    
    print('    ')


# Jogada do usuário
def jogadorJoga():
    global jogadas
    global quemJoga
    global maxJogadas

    if quemJoga == 2 and jogadas<maxJogadas :
    
        try: 
            l = int(input('Posição da '+ Fore.YELLOW + 'Linha: ' + Fore.RESET))
            c = int(input('Posição da '+ Fore.RED + 'Coluna: ' + Fore.RESET))

            while velha[l][c] != " ":
                l = int(input('Posição da '+ Fore.YELLOW + 'Linha: ' + Fore.RESET))
                c = int(input('Posição da '+ Fore.RED + 'Coluna: ' + Fore.RESET))
            velha[l][c]='X'
            quemJoga = 1
            jogadas += 1    # '+=' é o mesmo que || jogadas = jogadas+1 ||            
        except:
            print('Linha e/ou Coluna inválida!')
            time.sleep(1)
            #winner = False

# Jogada da CPU
def cpuJoga():
    global jogadas
    global quemJoga
    global maxJogadas

    if quemJoga == 1 and jogadas<maxJogadas :
        l = random.randrange(0,3)
        c = random.randrange(0,3)
        time.sleep(1)
        while velha[l][c] != " ":
            l = random.randrange(0,3)
            c = random.randrange(0,3)
            time.sleep(1)
        velha[l][c]='O'
        jogadas += 1
        quemJoga = 2

    
def verifVit():
    global velha
    winner = False
    simbolos = ['X','O']
    for s in simbolos:
        winner=False
        #verificando vitoria em linhas
        il=ic=0
        while il<3:
            soma=0
            ic=0
            while ic<3:
                if (velha[il][ic] == s):
                    soma += 1
                ic += 1
            
            if (soma == 3):
                winner = True
                break
            il += 1
        if winner != False:
            break

        #verificando vitoria em colunas
        il=ic=0
        while ic<3:
            soma=0
            il=0
            while il<3:
                if (velha[il][ic] == s):
                    soma += 1
                il += 1
          
            if (soma == 3):
                winner = True
                break
            ic += 1
        if winner != False:
            break
        
        #verificando Diagonal
        soma = 0 
        idiagL = 0
        idiagC = 2
        while idiagC >= 0 :
            if (velha[idiagL][idiagC] == s):
                soma += 1
            idiagL += 1
            idiagC -= 1
        if (soma == 3):
            winner = True
            break
    
    return winner

def redefinir():
    global playAgain
    global jogadas
    global quemJoga
    global maxJogadas
    global winner
    global velha


    playAgain = True    #Jogar Novamente
    jogadas = 0         #Quantidade de Jogadas
    quemJoga = 1        #1- vez da CPU , 2- minha vez
    maxJogadas = 9      #Quantidade máxima de jogadas
    winner = False       #Se ganhou ou perdeu
    

    #   Matriz do jogo
    velha = [
        [' ',' ',' '],
        [' ',' ',' '],
        [' ',' ',' ']
    ]
    


while True:
    tela()
    jogadorJoga()
    cpuJoga()
    vit = verifVit()
    if (vit!=False) or jogadas>=maxJogadas :
        if quemJoga == 2 :
            print(Fore.GREEN+'=========================')
            print('HUMANO VENCEU! PARABÉNS!')
            print('========================='+Fore.RESET)
            break
        else: 
            print(Fore.CYAN+'CPU VENCEU! MAIS SORTE NA PRÓXIMA!'+Fore.RESET)    
            break
    


    
    







